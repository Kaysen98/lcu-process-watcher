declare const EventEmitter: any;
interface LCUCredentials {
    host: string;
    port: string;
    username: string;
    password: string;
}
/**
 * LCUProcessWatcher is an event emitter, checking for presence of the lcu process and will emit events on open and close event of the lcu.
 *
 * @param {number} intervalInMilliseconds You can pass an interval in which the process watcher checks the process presence. Default is 1000 ms.
 */
declare class LCUProcessWatcher extends EventEmitter {
    private processWatcherInterval;
    private connected;
    private _intervalInMilliseconds;
    constructor(intervalInMilliseconds?: number);
    /**
     * Start the process watcher.
     * The event "connect" will be fired, when the process watcher finds a running lcu process.
     */
    start(): void;
    /**
     * Stop the process watcher.
     * The event "disconnect" will be fired, when the process watcher cant find a running lcu process anymore.
     * Only emits the event, when the process was present.
     */
    stop(): void;
    private getLCUPathFromProcess;
    private checkForProcess;
    private initProcessWatcherInterval;
    private clearProcessWatcherInterval;
}
export { LCUProcessWatcher, LCUCredentials, EventEmitter };
//# sourceMappingURL=index.d.ts.map