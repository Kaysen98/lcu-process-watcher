"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.EventEmitter = exports.LCUProcessWatcher = void 0;
const { EventEmitter } = require('events');
exports.EventEmitter = EventEmitter;
const child_process_1 = require("child_process");
const path = require("path");
const fs_1 = require("fs");
const IS_WIN = process.platform === 'win32';
const IS_MAC = process.platform === 'darwin';
/**
 * LCUProcessWatcher is an event emitter, checking for presence of the lcu process and will emit events on open and close event of the lcu.
 *
 * @param {number} intervalInMilliseconds You can pass an interval in which the process watcher checks the process presence. Default is 1000 ms.
 */
class LCUProcessWatcher extends EventEmitter {
    constructor(intervalInMilliseconds = 1000) {
        super();
        this.connected = false;
        this._intervalInMilliseconds = intervalInMilliseconds;
    }
    /**
     * Start the process watcher.
     * The event "connect" will be fired, when the process watcher finds a running lcu process.
     */
    start() {
        this.initProcessWatcherInterval();
    }
    /**
     * Stop the process watcher.
     * The event "disconnect" will be fired, when the process watcher cant find a running lcu process anymore.
     * Only emits the event, when the process was present.
     */
    stop() {
        this.clearProcessWatcherInterval();
    }
    getLCUPathFromProcess() {
        return new Promise(resolve => {
            const INSTALL_REGEX_WIN = /"--install-directory=(.*?)"/;
            const INSTALL_REGEX_MAC = /--install-directory=(.*?)( --|\n|$)/;
            const INSTALL_REGEX = IS_WIN ? INSTALL_REGEX_WIN : INSTALL_REGEX_MAC;
            const command = IS_WIN ?
                `WMIC PROCESS WHERE name='LeagueClientUx.exe' GET commandline` :
                `ps x -o args | grep 'LeagueClientUx'`;
            child_process_1.exec(command, (err, stdout, stderr) => {
                if (err || !stdout || stderr) {
                    resolve(null);
                    return;
                }
                const parts = stdout.match(INSTALL_REGEX) || [];
                resolve(parts[1]);
            });
        });
    }
    checkForProcess() {
        this.getLCUPathFromProcess().then((lcuPath) => {
            if (lcuPath) {
                if (!this.connected) {
                    const lockfilePath = path.join(lcuPath, 'lockfile');
                    fs_1.readFile(lockfilePath, (err, content) => {
                        const splitData = content.toString().split(':');
                        const result = {
                            host: '127.0.0.1',
                            port: splitData[2],
                            username: 'riot',
                            password: splitData[3],
                        };
                        this.connected = true;
                        this.emit('connect', result);
                    });
                }
            }
            if (!lcuPath) {
                if (this.connected) {
                    this.connected = false;
                    this.emit('disconnect');
                }
            }
        });
    }
    initProcessWatcherInterval() {
        setInterval(() => {
            this.checkForProcess();
        }, this._intervalInMilliseconds);
    }
    clearProcessWatcherInterval() {
        clearInterval(this.processWatcherInterval);
        this.processWatcherInterval = null;
    }
}
exports.LCUProcessWatcher = LCUProcessWatcher;
//# sourceMappingURL=index.js.map