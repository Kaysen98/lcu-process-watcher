const { EventEmitter } = require('events');
import { exec } from 'child_process';
import * as path from 'path';
import { readFile } from 'fs';

const IS_WIN = process.platform === 'win32';
const IS_MAC = process.platform === 'darwin';

interface LCUCredentials {
    host: string;
    port: string;
    username: string;
    password: string;
}

/**
 * LCUProcessWatcher is an event emitter, checking for presence of the lcu process and will emit events on open and close event of the lcu.
 * 
 * @param {number} intervalInMilliseconds You can pass an interval in which the process watcher checks the process presence. Default is 1000 ms.
 */
class LCUProcessWatcher extends EventEmitter {
    private processWatcherInterval: NodeJS.Timeout;
    private connected: boolean = false;
    private _intervalInMilliseconds: number;

    constructor(intervalInMilliseconds: number = 1000) {
        super();

        this._intervalInMilliseconds = intervalInMilliseconds;
    }

    /**
     * Start the process watcher.
     * The event "connect" will be fired, when the process watcher finds a running lcu process.
     */
    start() {
        this.initProcessWatcherInterval();
    }

    /**
     * Stop the process watcher.
     * The event "disconnect" will be fired, when the process watcher cant find a running lcu process anymore.
     * Only emits the event, when the process was present.
     */
    stop() {
        this.clearProcessWatcherInterval();
    }

    private getLCUPathFromProcess(): Promise<string> {
        return new Promise(resolve => {
            const INSTALL_REGEX_WIN = /"--install-directory=(.*?)"/;
            const INSTALL_REGEX_MAC = /--install-directory=(.*?)( --|\n|$)/;
            const INSTALL_REGEX = IS_WIN ? INSTALL_REGEX_WIN : INSTALL_REGEX_MAC;
            const command = IS_WIN ?
                `WMIC PROCESS WHERE name='LeagueClientUx.exe' GET commandline` :
                `ps x -o args | grep 'LeagueClientUx'`;

            exec(command, (err, stdout, stderr) => {
                if (err || !stdout || stderr) {
                    resolve(null);
                    return;
                }

                const parts = stdout.match(INSTALL_REGEX) || [];
                resolve(parts[1]);
            });
        });
    }

    private checkForProcess() {
        this.getLCUPathFromProcess().then((lcuPath) => {
            if (lcuPath) {
                if (!this.connected) {
                    const lockfilePath = path.join(lcuPath, 'lockfile');

                    readFile(lockfilePath, (err, content) => {
                        const splitData = content.toString().split(':');
            
                        const result: LCUCredentials = {
                            host: '127.0.0.1',
                            port: splitData[2],
                            username: 'riot',
                            password: splitData[3],
                        };
                        
                        this.connected = true;
                        this.emit('connect', result);
                    });
                }
            }

            if (!lcuPath) {
                if (this.connected) {

                    this.connected = false;
                    this.emit('disconnect');
                }
            }
        });
    }

    private initProcessWatcherInterval() {
        setInterval(() => {
            this.checkForProcess();
        }, this._intervalInMilliseconds);
    }

    private clearProcessWatcherInterval() {
        clearInterval(this.processWatcherInterval);
        this.processWatcherInterval = null;
    }
}

export { LCUProcessWatcher, LCUCredentials, EventEmitter }